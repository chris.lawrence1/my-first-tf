terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.52.0"
    }
  }
}

provider "aws" {
  region  = "eu-west-2"
}

resource "aws_s3_bucket" "bucket" {
  bucket = "ClorroDemo01"
}
